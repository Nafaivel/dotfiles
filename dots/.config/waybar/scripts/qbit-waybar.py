import json
from selenium import webdriver


def get_torrents(driver, response, category_xpath:str):
    element = driver.find_element_by_xpath(category_xpath)
    element.click()

    # get active torrents
    active_torrents = driver.find_elements_by_class_name("torrentsTableContextMenuTarget")
    formated_atorrents = []
    for i in range(len(active_torrents)):
        j = active_torrents[i].text[2:-1].split("\n")
        speed = driver.find_element_by_xpath("/html/body/div[1]/div[2]/div[3]/div[1]/div[2]/div/div[2]/table/tbody/tr[{}]/td[10]".format(i+1))
        # response["tooltip"]+=(j[0][:20]+"..."+j[1]+" "+speed.text+"\n")
        response["tooltip"]+=(j[0][:20]+"..."+j[1]+" "+speed.text+"\n")
    return response


qbiturl = "http://127.0.0.1:8080"
response = {
    "text": "x",
    "class": "",
    "tooltip": ""
}

# set heddless firefox
options = webdriver.firefox.options.Options()
options.headless = True

# run firefox
driver = webdriver.Firefox(options=options)

succ = False


try:
    # define url
    driver.get(qbiturl)
    succ = True
except:
    # uncoment if you want format disconnected
    # response['class']="disconnected"
    print(json.dumps(response))

if succ:
    response = get_torrents(driver, response, """/html/body/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/ul/li[5]/a""")
    # choose tracked category and go to it
    if response["tooltip"] != "":
        # check active category
        response["tooltip"] = response["tooltip"][0:-1]
        response["class"] = "active"
    else:
        # chech overed category
        response = get_torrents(driver, response, """/html/body/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/ul/li[3]/a""")
        if response["tooltip"] != "":
            response["tooltip"] = response["tooltip"][0:-1]
            response["class"] = "overed"
        else:
            # check stoped catgory
            response["class"] = "stoped"
    print(json.dumps(response,ensure_ascii=False))
    # close firefox
driver.quit()
