local naughty = require("naughty")
local beautiful = require("beautiful")
-- parse current theme
local theme_path = (beautiful.theme_path .. "theme")
-- remove path to awesome config dir
:gsub(string.format("%s/.config/awesome/", os.getenv("HOME")), "")
local theme = require(theme_path)

naughty.notify({
  title="bg_color",
  text=tostring(theme.bg_focus)})

theme.bg_focus = "#8d7d6c"

return theme
