local naughty = require("naughty")
local awful = require("awful")

awful.spawn.with_line_callback("kdeconnect-cli -a", {
    stdout = function(line)
        naughty.notify { title = "Kdeconnect:", text = line }
    end,
    stderr = function(line)
        naughty.notify { title = "Kdeconnect:", text = line}
    end,
})
