local awful = require("awful")

local M = {}
--- dont give common names like "python" it will kill it
M.killall = function (process_name)
  -- Kill processes with name "process_name"
  awful.spawn.with_shell("sleep 2 && killall " .. process_name)

  -- Get a list of process IDs with the name "process_name"
  awful.spawn.easy_async_with_shell(
    "sleep 2 && pgrep -f " .. process_name,
    function(stdout, stderr, exitreason, exitcode)
      -- Split the output of the pgrep command into a list of process IDs
      local pid_list = {}
      for pid in stdout:gmatch("%d+") do
        table.insert(pid_list, pid)
      end

      -- Kill child processes for each ID
      for _, pid in ipairs(pid_list) do
        awful.spawn.easy_async_with_shell(
          "pstree -p " .. pid,
          function(stdout, stderr, exitreason, exitcode)
            -- Parse the output of the pstree command to get the list of child process IDs
            local child_pid_list = {}
            for child_pid in stdout:gmatch("%d+") do
              if child_pid ~= pid then
                table.insert(child_pid_list, child_pid)
              end
            end

            -- Kill each child process
            for _, child_pid in ipairs(child_pid_list) do
              awful.spawn.with_shell("kill " .. child_pid)
            end
          end
        )
      end
    end
  )
end

return M
