local awful = require("awful")
local homedir = os.getenv("HOME")

local killall = require("lua.autostart.site").killall

killall("python "..homedir.."/.config/xob/pipewire-volume-watcher")
killall("python "..homedir.."/.config/xob/brightness-watcher")
killall("blueman-applet")
killall("blueman-tray")

-- volume display
awful.spawn.easy_async_with_shell("sleep 5 && $HOME/.config/xob/site/pipewire-volume-watcher.py | xob")
-- brightness display
awful.spawn.easy_async_with_shell("sleep 5 && $HOME/.config/xob/site/brightness-watcher.py | xob")
-- my startpage
awful.spawn.easy_async_with_shell("cd $HOME/.config/startpage && python -m http.server 6868")
--  syncthing (for file syncing)
awful.spawn.easy_async_with_shell("syncthing serve --no-browser > ~/.config/syncthing/syncthing.log")
-- noisy - utility for noisyng trafik
-- awful.spawn.easy_async("python ~/noisy/noisy.py --config config.json")

awful.spawn.easy_async_with_shell("emacs --daemon")

awful.spawn.easy_async_with_shell("~/.config/awesome/scripts/autostart.sh")
