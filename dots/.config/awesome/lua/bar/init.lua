local M = {}

-- Widget and layout library
local wibox = require("wibox")
local vicious = require("vicious")
local naughty = require("naughty")
local inspect = require("inspect")
local awful = require("awful")
local gears = require("gears")
local apps = require("lua.apps").setup()
local look = require("lua.look")
local keys = require("lua.keymaps")

local function get_edite_cfg()
  local editor_cfg = apps.editor_cmd .. " " .. awesome.conffile
  if apps.editor_cmd == apps.terminal .. " -e " .. "nvim" or apps.editor_cmd == apps.terminal .. " -e " .. "vim" then
    editor_cfg = editor_cfg .. ' "+cd ' .. awesome.conffile:gsub("rc.lua", "") .. '"'
  end
  return editor_cfg
end

M.mainmenu = awful.menu({
  items = {
    {"lock", "~/.config/i3/scripts/lock.sh"},
    {
      "edit config",
      get_edite_cfg(),
    },
    { "open terminal", apps.terminal },
    { "poweroff",      "poweroff" },
    { "reboot",        "reboot" },
    { "restart",       awesome.restart },
    { "quit",          "killall awesome" },
  },
})

function M.setup()
  -- Create widgets
  -- local lain = require("lain")
  local widgets_path = "lua.bar.awesome-wm-widgets."
  local battery_widget = require(widgets_path .. "battery-widget.battery")
  local bluetooth_widget = require(widgets_path .. "bluetooth-widget")
  -- local network_widget = require(widgets_path .. "network-widget")
  local calendar_widget = require(widgets_path .. "calendar-widget.calendar")
  local volume_widget = require(widgets_path .. "volume-widget.volume")
  --[[ local net_speed_widget = require(widgets_path .. "net-speed-widget.net-speed") ]]

  -- local org_box = wibox.widget.textbox()
  -- local org_widget = vicious.register(
  --    org_box,
  --    vicious.widgets.org,
  --    "${today} Today, ${soon} Soon, ${future} Future",
  --    { "/home/antasa/Documents/notes/org/refile.org" }
  -- )

  -- local wifi_box = wibox.widget.textbox()
  -- local wifi_widget = vicious.register(wifi_box, vicious.widgets.wifiiw, "󰖩 ${ssid}", 1, "wlan0")

  local function show_ip()
    awful.spawn.easy_async([[bash -c 'curl ifconfig.me']], function(stdout, _, _, _)
      naughty.destroy(notification)
      notification = naughty.notify({
        text = stdout,
        title = "IP",
        position = position,
        timeout = 5,
        hover_timeout = 0.5,
        width = 200,
        screen = mouse.screen,
      })
    end)
  end

  local wifi_box = wibox.widget.textbox()
  local wifi_widget = vicious.register(wifi_box, vicious.widgets.wifiiw, function(widget, args)
    local ssid = args["{ssid}"]
    local ico = ""
    
    -- naughty.notify({
    -- title = "test",
    -- text = inspect(args),
    -- })

    -- Check if the SSID is available
    if ssid == "N/A" then
      ssid = ""
    else
      ico = "󰖩 "
    end

  -- Set the text for the widget
  widget:set_text(ico .. ssid)
  end, 1, "wlan0")

  -- local eth_box = wibox.widget.textbox()
  -- local eth_widget = vicious.register(eth_box, vicious.widgets.net, "󰈀${eno1 carrier}")
  local eth_box = wibox.widget.textbox()
  local eth_widget = vicious.register(eth_box, vicious.widgets.net, function(widget, args)
      if args["{eno1 carrier}"] == 1 then
	return "󰈀 "
      else
	return ""
      end
  end)

  eth_box:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then
       show_ip()
    elseif button == 2 then
      awful.spawn("firewall-config")
    elseif button == 3 then
      awful.spawn("networkmanager_dmenu")
    end
  end)

  wifi_box:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then
       show_ip()
    elseif button == 2 then
      awful.spawn("firewall-config")
    elseif button == 3 then
      awful.spawn("networkmanager_dmenu")
    end
  end)

  local systray = wibox.widget.systray()
  systray:set_base_size(25)

  naughty.config.defaults.icon_size = 32

  -- Keyboard map indicator and switcher
  local mykeyboardlayout = awful.widget.keyboardlayout()

  -- Create a textclock widget
  local mytextclock = wibox.widget.textclock("%d.%m_%H:%M")
  -- customized
  local cw = calendar_widget({
    theme = "dark",
    placement = "top_right",
    start_sunday = false,
    radius = 8,
    -- with customized next/previous (see table above)
    previous_month_button = 3,
    next_month_button = 1,
  })

  mytextclock:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then
      cw.toggle()
    end
    --[[ if button == 3 then ]]
    --[[   awful.spawn("evolution") ]]
    --[[ end ]]
  end)

  -- Create a wibox for each screen and add it
  local taglist_buttons = gears.table.join(
    awful.button({}, 1, function(t)
      t:view_only()
    end),
    awful.button({ keys.modkey }, 1, function(t)
      if client.focus then
        client.focus:move_to_tag(t)
      end
    end),
    awful.button({}, 3, awful.tag.viewtoggle),
    awful.button({ keys.modkey }, 3, function(t)
      if client.focus then
        client.focus:toggle_tag(t)
      end
    end),
    awful.button({}, 4, function(t)
      awful.tag.viewprev(t.screen)
    end),
    awful.button({}, 5, function(t)
      awful.tag.viewnext(t.screen)
    end)
  )

  local tasklist_buttons = gears.table.join(
    awful.button({}, 1, function(c)
      c:emit_signal("request::activate", "tasklist", { raise = true })
    end),
    awful.button({}, 3, function(c)
      c.minimized = true
      -- awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({}, 4, function()
      awful.client.focus.byidx(1)
    end),
    awful.button({}, 5, function()
      awful.client.focus.byidx(-1)
    end)
  )

  -- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
  screen.connect_signal("property::geometry", look.set_wallpaper)

  awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    look.set_wallpaper(s)

    -- Each screen has its own tag table.
    --[[ awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1]) ]]
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
      awful.button({}, 1, function()
        awful.layout.inc(1)
      end),
      awful.button({}, 3, function()
        awful.layout.inc(-1)
      end),
      awful.button({}, 4, function()
        awful.layout.inc(1)
      end),
      awful.button({}, 5, function()
        awful.layout.inc(-1)
      end)
    ))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist({
      screen = s,
      filter = awful.widget.taglist.filter.all,
      buttons = taglist_buttons,
    })

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist({
      screen = s,
      filter = awful.widget.tasklist.filter.currenttags,
      buttons = tasklist_buttons,
      --[[ style = { ]]
      --[[ 	border_width = 1, ]]
      --[[ 	border_color = "#777777", ]]
      --[[ 	shape = gears.shape.rounded_bar, ]]
      --[[ }, ]]
      --[[ layout = { ]]
      --[[ 	spacing = 10, ]]
      --[[ 	spacing_widget = { ]]
      --[[ 		{ ]]
      --[[ 			forced_width = 5, ]]
      --[[ 			shape = gears.shape.circle, ]]
      --[[ 			widget = wibox.widget.separator, ]]
      --[[ 		}, ]]
      --[[ 		valign = "center", ]]
      --[[ 		halign = "center", ]]
      --[[ 		widget = wibox.container.place, ]]
      --[[ 	}, ]]
      --[[ 	layout = wibox.layout.flex.horizontal, ]]
      --[[ }, ]]
      -- Notice that there is *NO* wibox.wibox prefix, it is a template,
      -- not a widget instance.
      widget_template = {
        {
          {
            {
              id = "icon_role",
              widget = wibox.widget.imagebox,
            },
            margins = 6.25,
            widget = wibox.container.margin,
          },
          {
            id = "text_role",
            widget = wibox.widget.textbox,
          },
          layout = wibox.layout.fixed.horizontal,
        },
        id = "background_role",
        widget = wibox.container.background,
      },
    })

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup({
      layout = wibox.layout.align.horizontal,
      {
        -- Left widgets
        layout = wibox.layout.fixed.horizontal,
        -- mylauncher,
        s.mytaglist,
        s.mypromptbox,
      },
      s.mytasklist, -- Middle widget
      {
        -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        mykeyboardlayout,
        -- widget, left, right, top, bottom
	org_widget,
        battery_widget({
          display_notification = false,
          margin_left = 0,
          margin_right = 5,
        }),
        --[[ network_widget({ ]]
        --[[   margin_left = 0, ]]
        --[[   margin_right = 5, ]]
        --[[   show_wifi_name = true, ]]
        --[[   timeout = 0.1 ]]
        --[[ }), ]]
        eth_widget,
        wifi_widget,
        --[[ wibox.container.margin(net_speed_widget(), 3, 15, 0, 0), ]]
        -- widget, left, right, top, bottom
        volume_widget({
          --[[ widget_type = "icon_and_text", ]]
          widget_type = "icon",
          with_icon = true,
          timeout = 0.1,
          margin_left = 5,
          margin_right = 5,
        }),
        bluetooth_widget({
          timeout = 0.1,
          margin_left = 0,
          margin_right = 0,
        }),
        wibox.container.margin(systray, 0, 3, 4, 0),
        mytextclock,
        wibox.container.margin(s.mylayoutbox, 3, 3, 5, 5),
      },
    })
  end)
  return M
end

return M
