local awful = require("awful")
local naughty = require("naughty")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local gfs = require("gears.filesystem")
local dpi = require("beautiful").xresources.apply_dpi

local bluetooth_widget = {}
local function worker(user_args)
  local args = user_args or {}

  local path_to_icons = args.path_to_icons or "/usr/share/icons/Papirus-Dark/symbolic/status/"
  --[[ local path_to_icons = args.path_to_icons or "/usr/share/icons/Papirus-Light/symbolic/status/" ]]
  local margin_left = args.margin_left or 0
  local margin_right = args.margin_right or 0

  local display_notification = args.display_notification or false
  local display_notification_onClick = args.display_notification_onClick or true
  local timeout = args.timeout or 10
  local bluetooth_menu_command = args.bluetooth_menu_command or nil
  local bluetooth_menu_command_alt = args.bluetooth_menu_command_alt or nil

  if not gfs.dir_readable(path_to_icons) then
    naughty.notify({
      title = "Bluetooth Widget",
      text = "Folder with icons doesn't exist: " .. path_to_icons,
      preset = naughty.config.presets.critical,
    })
  end

  local icon_widget = wibox.widget({
    {
      id = "icon",
      widget = wibox.widget.imagebox,
      resize = false,
    },
    valign = "center",
    layout = wibox.container.place,
  })
  bluetooth_widget = wibox.widget({
    icon_widget,
    layout = wibox.layout.fixed.horizontal,
  })
  -- Popup with bluetooth info
  -- One way of creating a pop-up notification - naughty.notify
  local function get_status_text(stdout)
    stdout = stdout:match('^(.*)\n')
    if stdout == "no" then
      return "off"
    elseif stdout == "yes" then
      return "on"
    else
      return stdout
    end
  end

  local notification
  local function show_bluetooth_status(bluetoothType)
    awful.spawn.easy_async([[bash -c 'bluetoothctl show | rg Powered | cut -d " " -f2-']], function(stdout, _, _, _)
      naughty.destroy(notification)
      notification = naughty.notify({
        text = get_status_text(stdout),
        title = "Bluetooth status",
        icon = path_to_icons .. bluetoothType .. ".svg",
        icon_size = dpi(16),
        timeout = 5,
        hover_timeout = 0.5,
        width = 200,
        screen = mouse.screen,
      })
    end)
  end

  local bluetoothType = "bluetooth-disabled-symbolic"

  watch([[bash -c 'bluetoothctl show | rg Powered | cut -d " " -f2-']], timeout, function(widget, stdout)
    local bluetooth_info = {}
    bluetooth_info.status = get_status_text(stdout)

    local status = bluetooth_info.status

    if status == "on" then
      bluetoothType = "bluetooth-active-symbolic"
    elseif status == "off" then
      bluetoothType = "bluetooth-disabled-symbolic"
    end

    widget.icon:set_image(path_to_icons .. bluetoothType .. ".svg")

    -- Update popup text
    -- battery_popup.text = string.gsub(stdout, "\n$", "")
  end, icon_widget)

  if display_notification then
    bluetooth_widget:connect_signal("mouse::enter", function()
      show_bluetooth_status(bluetoothType)
    end)
    bluetooth_widget:connect_signal("mouse::leave", function()
      naughty.destroy(notification)
    end)
  elseif display_notification_onClick then
    bluetooth_widget:connect_signal("button::press", function(_, _, _, button)
      if button == 1 then
        show_bluetooth_status(bluetoothType)
      end
      if button == 2 then
        if bluetooth_menu_command_alt then
          awful.spawn.with_shell(bluetooth_menu_command_alt)
        end
      end
      if button == 3 then
        if bluetooth_menu_command then
          awful.spawn.with_shell(bluetooth_menu_command)
        end
      end
    end)
    bluetooth_widget:connect_signal("mouse::leave", function()
      naughty.destroy(notification)
    end)
  end

  return wibox.container.margin(bluetooth_widget, margin_left, margin_right)
end

return setmetatable(bluetooth_widget, {
  __call = function(_, ...)
    return worker(...)
  end,
})
