# awesome-client '
#   local wibox = require("wibox")
#   local vicious = require("vicious")
#   local naughty = require("naughty")
#   local inspect = require("inspect")

#   local org_box = wibox.widget.textbox()
#   local org_widget = vicious.register(
#      org_box,
#      vicious.widgets.org,
#      "$1",
#      { "/home/antasa/Documents/notes/org/refile.org" }
#   )

# '

awesome-client '
  local wibox = require("wibox")
  local vicious = require("vicious")
  local naughty = require("naughty")
  local inspect = require("inspect")
  local beautiful = require("beautiful")
  local awful = require("awful")
  local gears = require("gears")

    awful.popup {
        widget = {
            {
                {
                    text   = "foobar",
                    widget = wibox.widget.textbox
                },
                {
                    {
                        text   = "foobar",
                        widget = wibox.widget.textbox
                    },
                    bg     = "#ff00ff",
                    clip   = true,
                    shape  = gears.shape.rounded_bar,
                    widget = wibox.widget.background
                },
                {
                    value         = 0.5,
                    forced_height = 30,
                    forced_width  = 100,
                    widget        = wibox.widget.progressbar
                },
                layout = wibox.layout.fixed.vertical,
            },
            margins = 10,
            widget  = wibox.container.margin
        },
        border_color = "#00ff00",
        border_width = 5,
        placement    = awful.placement.top_left,
        shape        = gears.shape.rounded_rect,
        visible      = true,
    }
'
