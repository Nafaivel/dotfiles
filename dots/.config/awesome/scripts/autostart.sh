#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    echo "$@"
    "$@"&
  fi
}

# Network Manager applet
# run "nm-applet"
# Flameshot
# run "flameshot"
# music play daemon
run "mpd" 
run "mpDris2"
run "playerctld"
# start searx - internet search engine
# run "searx-run"
#polkit
run "lxsession"
#kde conncect
run "/usr/lib/kdeconnectd"
run "indicator-kdeconnect"
# start gestures daemon
run "libinput-gestures"
# store passwds
run "kwalletd5"
# caldav
# run "radicale"
# transperensy
run "picom"
# open tablet driver
run "otd"
# track clip
run "gpaste-client start"
killall nm-applet
