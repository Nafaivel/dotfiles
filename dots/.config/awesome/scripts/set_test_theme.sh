awesome-client '
local beautiful = require("beautiful")
local theme_path = string.format("%s/.config/awesome/themes/test_theme.lua", os.getenv("HOME"))
beautiful.init(theme_path)
'
