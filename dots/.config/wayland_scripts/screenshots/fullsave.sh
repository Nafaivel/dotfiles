#!/bin/bash

path="$HOME/Pictures/Screenshots/$(date +%Y-%m-%d_%H-%M-%S).png"
sleep 2 && grim "$path" && notify-send "screenshot saved" "$path" -u low
