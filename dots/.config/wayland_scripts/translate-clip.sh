#!/bin/bash

text=$(wl-paste)
translated_text=$(trans \
  -show-original n\
  -show-original-phonetics n\
  -show-prompt-message n\
  -show-languages n\
  -no-ansi\
  :be "$text"
)

# NOTE: Trim clip len to 37 chars to prevent linebreak in mako for my setup. Feel free to update
notify-send -u low "${text:0:37}" "$translated_text"
