# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

import qbc.draw as draw
import sys
import os

# # pylint: disable=C0111
# from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
# from qutebrowser.config.config import ConfigContainer  # noqa: F401
# config: ConfigAPI = config  # noqa: F821 pylint: disable=E0602,C0103
# c: ConfigContainer = c  # noqa: F821 pylint: disable=E0602,C0103 

from typing import TYPE_CHECKING, cast

if TYPE_CHECKING:
    from qutebrowser.config.configfiles import ConfigAPI
    from qutebrowser.config.config import ConfigContainer

    # note: these expressions aren't executed at runtime
    c = cast(ConfigContainer, ...)
    config = cast(ConfigAPI, ...)

#            __            _
#     ____  / /_  ______ _(_)___  _____
#    / __ \/ / / / / __ `/ / __ \/ ___/
#   / /_/ / / /_/ / /_/ / / / / (__  )
#  / .___/_/\__,_/\__, /_/_/ /_/____/
# /_/            /____/
# plugins

sys.path.append(os.path.join(sys.path[0], "modules", "jmatrix"))
config.source("modules/jmatrix/jmatrix/integrations/qutebrowser.py")
config.source("modules/redirrect.py")

#                __
#   ____  ____  / /______
#  / __ \/ __ \/ __/ ___/
# / /_/ / /_/ / /_(__  )
# \____/ .___/\__/____/
#     /_/
# opts

config.load_autoconfig(False)
c.auto_save.session = True

# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only. This is known to break some sites, such as GMail.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
#   - never: Don't accept cookies at all.
config.set("content.cookies.accept", "all", "chrome-devtools://*")
config.set("content.cookies.accept", "all", "devtools://*")

# Value to send in the `Accept-Language` header. Note that the value
# read from JavaScript is always the global value.
# Type: String
config.set("content.headers.accept_language", "", "https://matchmaker.krunker.io/*")
c.content.headers.accept_language = "en-US,en;q=0.5"
c.content.headers.custom = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
}


# useragent
c.content.headers.user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36"

# spellcheck langs
c.spellcheck.languages = ["ru-RU", "en-US"]

# block
c.content.blocking.hosts.lists = [
    "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts",
    "http://sbc.io/hosts/alternates/fakenews-gambling-porn-social/hosts",
]
c.content.blocking.method = "both"

# rendering
c.content.webgl = False
c.content.canvas_reading = False
c.qt.highdpi = True


# Editor (and arguments) to use for the `edit-*` commands. The following
# placeholders are defined:  * `{file}`: Filename of the file to be
# edited. * `{line}`: Line in which the caret is found in the text. *
# `{column}`: Column in which the caret is found in the text. *
# `{line0}`: Same as `{line}`, but starting from index 0. * `{column0}`:
# Same as `{column}`, but starting from index 0.
# Type: ShellCommand
# c.editor.command = ["emacsclient", "-c", "{file}"]
ed_cmd = os.getenv("GUI_EDITOR")
if ed_cmd is None:
    ed_cmd = "$TERMINAL -e nvim"

c.editor.command = ed_cmd.split(" ") + ["{file}"]

# new and start page
c.url.default_page = "127.0.0.1:6868"
c.url.start_pages = "127.0.0.1:6868"

# hints
c.hints.uppercase = True
c.fonts.hints = "regular 14 FiraCode Nerd Font"
c.fonts.default_size = "14pt"

# lazy loading for tabs
c.session.lazy_restore = True

# close tab on right click
config.set("tabs.close_mouse_button", "right")
#                               __
#    ________  ____ ___________/ /_
#   / ___/ _ \/ __ `/ ___/ ___/ __ \
#  (__  )  __/ /_/ / /  / /__/ / / /
# /____/\___/\__,_/_/   \___/_/ /_/
# search

# yt_url = "yt.artemislena.eu"
# yt_url = "invidious.nerdvpn.de"
yt_url = "yewtu.be"

c.url.searchengines = {
    # common search engines
    # "DEFAULT": "https://searx.be/search?q={}",
    "DEFAULT": "https://duckduckgo.com/?t=h_&q={}&ia=web",
    "ddg": "https://duckduckgo.com/?t=h_&q={}&ia=web",
    "ggl": "https://google.com/search?q={}",
    "sp": "https://www.startpage.com/sp/search?q={}",
    "sx": "https://searx.be/search?q={}",
    "sxng": "https://searx.hu/search?q={}",
    "mj": "https://www.mojeek.com/search?q={}",
    "qw": "https://www.qwant.com/?q={}",
    # Youtube
    "yt": "https://" + yt_url + "/search?q={}",
    # Sciense
    "pm": "https://pubmed.ncbi.nlm.nih.gov/?term={}",
    # dev
    "cr": "https://crates.io/search?q={}",
    "crr": "https://crates.io/crates/{}",
    "cd": "https://docs.rs/{}",
    "dh": "https://hub.docker.com/search?q={}",
    # Translator
    "ruen": "https://www.deepl.com/translator#ru/en/{}",
    "enru": "https://www.deepl.com/translator#en/ru/{}",
    "been": "https://translate.google.com/?sl=be&tl=en&text={}&op=translate",
    "enbe": "https://translate.google.com/?sl=en&tl=be&text={}&op=translate",
    "be": "https://slounik.org/search?dict=&search={}",
    "beterm": "https://www.skarnik.by/search?term={}&lang=beld",
    "beverb": "https://verbum.by/?q={}",
    "beru": "https://www.skarnik.by/search?term={}&lang=bel",
    "rube": "https://www.skarnik.by/search?term={}&lang=rus",
    # Books
    "flibusta": "http://flibusta.is/booksearch?ask={}",
    # Wiki
    "aw": "https://wiki.archlinux.org/index.php?search={}",
    "au": "https://aur.archlinux.org/packages?K={}",
    # Emacs
    "melpa": "https://melpa.org/#/?q={}",
}

#               __
#   _________  / /___  __________
#  / ___/ __ \/ / __ \/ ___/ ___/
# / /__/ /_/ / / /_/ / /  (__  )
# \___/\____/_/\____/_/  /____/

bg = "#44475a"
bg_pinned = bg
fg = "#ffffff"
selected_bg = "#000000"
selected_fg = "#ffffff"

# pinned even
c.colors.tabs.pinned.even.bg = bg_pinned
c.colors.tabs.pinned.even.fg = fg
c.colors.tabs.pinned.selected.even.bg = selected_bg
c.colors.tabs.pinned.selected.even.fg = selected_fg

# pinned odd
c.colors.tabs.pinned.odd.bg = bg_pinned
c.colors.tabs.pinned.odd.fg = fg
c.colors.tabs.pinned.selected.odd.bg = selected_bg
c.colors.tabs.pinned.selected.odd.fg = selected_fg

# even
c.colors.tabs.even.bg = bg
c.colors.tabs.even.fg = fg
c.colors.tabs.selected.even.bg = selected_bg
c.colors.tabs.selected.even.fg = selected_fg

# odd
c.colors.tabs.odd.bg = bg
c.colors.tabs.odd.fg = fg
c.colors.tabs.selected.odd.bg = selected_bg
c.colors.tabs.selected.odd.fg = selected_fg

# Hints
c.colors.hints.bg = "#000000"
c.colors.hints.fg = "#7FFFD4"


# dracula
draw.blood(
    c,
    {
        # 'spacing': {
        #     'vertical': 6,
        #     'horizontal': 8
        # }
    },
)

c.colors.webpage.preferred_color_scheme = "light"
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.darkmode.algorithm = "lightness-cielab"
c.colors.webpage.darkmode.contrast = -0.022
c.colors.webpage.darkmode.threshold.foreground = 150
c.colors.webpage.darkmode.threshold.background = 100
c.colors.webpage.darkmode.policy.images = "smart-simple"
# c.colors.webpage.darkmode.grayscale.images = 0.35
c.colors.webpage.bg = "black"
pages_css_path = "~/.config/qutebrowser/css/catppuccin-dark-all-sites.css"
c.content.user_stylesheets = pages_css_path

c.window.transparent = True

#          ___
#   ____ _/ (_)___ _________  _____
#  / __ `/ / / __ `/ ___/ _ \/ ___/
# / /_/ / / / /_/ (__  )  __(__  )
# \__,_/_/_/\__,_/____/\___/____/
# aliases

c.aliases = {"q": "close", "sou": "config-source", "w": "session-save a"}

#     ____  _           ___
#    / __ )(_)___  ____/ (_)___  ____ ______
#   / __  / / __ \/ __  / / __ \/ __ `/ ___/
#  / /_/ / / / / / /_/ / / / / / /_/ (__  )
# /_____/_/_/ /_/\__,_/_/_/ /_/\__, /____/
#                             /____/
# Bindings

# langmap
key_mappings = {
    "<Ctrl-6>": "<Ctrl-^>",
    "<Ctrl-Enter>": "<Ctrl-Return>",
    "<Ctrl-J>": "<Tab>",
    "<Ctrl-K>": "<Up>",
    "<Ctrl-L>": "<Return>",
    "<Ctrl-Shift-v>": "<Ctrl-v>",
    "<Ctrl-[>": "<Escape>",
    "<Enter>": "<Return>",
    "<Shift-Enter>": "<Return>",
    "<Shift-Return>": "<Return>",
}

what_map = [
    "ёйцукенгшўзхъфывапролджэячсмітьбю",
    "ËЙЦУКЕНГШЎЗХЪФЫВАПРОЛДЖЭЯЧСМІТЬБЮ",
]

to_what_map = [
    "`qwertyuiop[]asdfghjkl;'zxcvbnm,.",
    '~QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>',
]

for pair_i in range(0, len(what_map)):
    for key_i in range(0, len(what_map[pair_i])):
        map_key = what_map[pair_i][key_i]
        # should always be 0 or 1
        pair_i = pair_i + 1 // 2
        to_map_key = to_what_map[pair_i][key_i]
        # print(map_key + " fake-key " + to_map_key, i)
        key_mappings[map_key] = to_map_key
        # config.bind(map_key, "fake-key "+to_map_key, mode='normal')

c.bindings.key_mappings = key_mappings

# navigating
config.bind("j", ":cmd-run-with-count 4 scroll down")
config.bind("k", ":cmd-run-with-count 4 scroll up")

# Quick copy link
config.bind("yy", "hint links spawn ~/.config/qutebrowser/scripts/copy_args.sh {hint-url}")

# Bookmarks
config.bind("aa", "cmd-set-text -s :quickmark-add '{url}' '{title}' ")

# Tabs
config.bind("gm", "tab-move")
config.bind("gM", "cmd-set-text -s :tab-move")
config.bind("gp", "tab-pin")

# space keys
# MPV
config.bind(" M", "spawn -d mpv {url}")
config.bind(" m", "hint links spawn -d mpv {hint-url}")
# MPD
config.bind(" na", "hint links spawn -d ~/.dots/bin_scripts/mpd_by_url.sh add {hint-url}")
config.bind(" nA", "spawn -d ~/.dots/bin_scripts/mpd_by_url.sh add {url}")
config.bind(" nn", "hint links spawn -d ~/.dots/bin_scripts/mpd_by_url.sh add {hint-url}")
config.bind(" nN", "spawn -d ~/.dots/bin_scripts/mpd_by_url.sh insert {url}")
config.bind(" ni", "hint links spawn -d ~/.dots/bin_scripts/mpd_by_url.sh insert {hint-url}")
config.bind(" nI", "spawn -d ~/.dots/bin_scripts/mpd_by_url.sh insert {url}")
# open pdf
config.bind(" p", "hint links spawn zathura {hint-url}")
config.bind(" i", "hint links spawn feh {hint-url}")
config.bind(" c", "hint links spawn ~/.dots/bin_scripts/copy_file_by_url_to_clip.sh {hint-url}")
# Edit
config.bind(" ej", "jmatrix-edit-config")
config.bind(" ec", "config-edit")
# Jmatrix
config.bind(" jr", "jmatrix-read-config")
config.bind(" jw", "jmatrix-write-config")
config.bind(" jT", "jmatrix-toggle")
config.bind(" jt", "cmd-set-text -s :jmatrix-toggle-rule")
# sessions
config.bind("sss", "cmd-set-text -s :session-save")
config.bind("ssl", "cmd-set-text -s :session-load")
config.bind("ssd", "cmd-set-text -s :session-delete")
# sets (toggles)
config.bind("sr", "redirect-toggle")
config.bind("sj", "jmatrix-toggle")
config.bind("sdd", "config-cycle colors.webpage.darkmode.enabled")
config.bind("sdc", "config-cycle content.user_stylesheets " + pages_css_path + " ~/.config/qutebrowser/css/empty.css",)
config.bind("sdi", "config-cycle colors.webpage.darkmode.policy.images smart always never")

# clip
config.bind("<Ctrl+Shift+c>", "spawn --userscript ~/.config/qutebrowser/scripts/copy_selected_text.sh", mode="insert",)
config.bind("<Ctrl+Shift+c>", "spawn --userscript ~/.config/qutebrowser/scripts/copy_selected_text.sh", mode="normal",)
config.bind("<Ctrl+c>", "spawn --userscript ~/.config/qutebrowser/scripts/copy_selected_text.sh", mode="insert",)
config.bind("<Ctrl+c>", "spawn --userscript ~/.config/qutebrowser/scripts/copy_selected_text.sh", mode="normal",)
