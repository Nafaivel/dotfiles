#!/bin/zsh

if [[ $XDG_SESSION_TYPE == "wayland" ]]; then
copy_cmd="wl-copy"
else
copy_cmd="xsel -b -i"
fi

echo -n "$QUTE_SELECTED_TEXT" | $copy_cmd
