#!/bin/bash

catppuccincss_dir="$HOME/development/css/catppuccin-all-css"

confirm()
{
  read -p "$@ " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    return 0
  else
    return 1
  fi
}

confirm "Delete qbc and jmatrix dirs?" && rm -r ./qbc ./modules/jmatrix
confirm "Git clone qbc and jmatrix?" && git clone "https://github.com/dracula/qutebrowser.git" ./qbc && git clone "https://github.com/mirryi/jmatrix.git" ./modules/jmatrix
confirm "Install catppuccin css?" && git clone "https://gitlab.com/Nafaivel/catppuccin-all-css.git" $catppuccincss_dir && cd $catppuccincss_dir && $catppuccincss_dir/build.sh && ln -sf $catppuccincss_dir/style.css $HOME/.config/qutebrowser/css/catppuccin-dark-all-sites.css
