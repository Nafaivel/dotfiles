[colors]
bg = #60000000
bg_mod = #c0000000
main = ${colors.blue}
secondary = ${colors.bblue}
warning = ${colors.red}
line = ${colors.main}
red = #ff2052
pink = #ec3b83
bpink = #e4717a
warn_pink = #ffb7c5
blue = #0892d0
bblue = #00ffff

[bar/2nd_monitor]
modules-right = pipiwire battery date
modules-left = i3 
background = #a0000000
monitor = HDMI-1-0


[bar/top]
modules-right = wlan xkeyboard pulseaudio battery date
modules-left = i3 xwindow 
modules-center = 

padding-right = 1
module-margin-left = 1

background = ${colors.bg}
monitor = eDP

tray-position = center
tray-padding = 1
tray-offset-x = 0
tray-background = ${colors.bg}

font-0 = FiraCode Nerd Font:pixelsize=12;2
font-1 = siji:pixelsize=11;1
font-2 = unifont:fontformat=truetype:size=8:antialias=false;0
font-3 = Typicons:size=16;2
font-4 = NotoColorEmoji:fontformat=truetype:scale=10;3

;-------------------------------------------------wlan
[module/wlan]
type = internal/network
interface = wlan0
interval = 3.0
format-connected = <label-connected>
label-connected =  ⟨%essid%⟩
label-connected-foreground = #ffffff
label-connected-background = ${colors.blue}
label-connected-padding = 1
ramp-signal-foreground = #ffffff

;-------------------------------------------------xwindow
[module/xwindow]
type = internal/xwindow
label = %title:0:40:...%
format-background = ${colors.line}

;-------------------------------------------------nm-stuff
[module/network]
type = internal/network
interface = wlp4s0
format-connected = %{A1:$HOME/.config/polybar/Scripts/network/nmcli-rofi:}<ramp-signal>%{A}
format-disconnected = %{A1:$HOME/.config/polybar/Scripts/network/nmcli-rofi:}%{A}
click-left= 
click-right=$HOME/.config/rofi/Scripts/network/nmcli-rofi

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 

;-------------------------------------------------xkeyboard
[module/xkeyboard]
type = internal/xkeyboard
interval = 0
format-background=#ab708090
format-label = <label>
label-layout =  %layout%
label-indicator-on-numlock =
label-indicator-on-capslock =

;-------------------------------------------------date
[module/date]
type = internal/date
; Seconds to sleep between updates
interval = 1.0
date = %d-%m-%y
time = %H:%M

; Available tags:
;   <label> (default)
format = <label>
format-background = ${colors.bg_mod}
format-foreground = ${colors.secondary}

; Available tokens:
;   %date%
;   %time%
; Default: %date%
label =  %date% %time%
label-padding = 1
label-font = 1

;-------------------------------------------------pulseaudio
[module/pulseaudio]
type = internal/pulseaudio

; Sink to be used, if it exists (find using `pacmd list-sinks`, name field)
; If not, uses default sink
sink = alsa_output.pci-0000_12_00.3.analog-stereo

; Use PA_VOLUME_UI_MAX (~153%) if true, or PA_VOLUME_NORM (100%) if false
use-ui-max = false

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-foreground = #ffffff

; Interval for volume increase/decrease (in percent points)
interval = 5
; Available tags:
;   <label-volume> (default)
;   <ramp-volume>
;   <bar-volume>
format-volume = %{B#aa007aa5} <ramp-volume> <label-volume> %{B-}

; Available tags:
;   <label-muted> (default)
;   <ramp-volume>
;   <bar-volume>
format-muted = 婢 <label-muted>

; Available tokens:
;   %percentage% (default)
;   %decibels%
;label-volume = %percentage%%

; Available tokens:
;   %percentage% (default)
;   %decibels%
format-muted-padding = 1
format-muted-foreground = #b2beb5
format-muted-background = #848482

; Right and Middle click
click-right = pavucontrol
; click-middle =

;-------------------------------------------------Pipewire
[module/pipewire]
type = custom/script
label = "%output%"
label-font = 2
interval = 2.0
exec = ~/.config/polybar/scripts/pipewire.sh
click-right = exec pavucontrol &
click-left = ~/.config/polybar/pipewire.sh mute &
scroll-up = ~/.config/polybar/pipewire.sh up &
scroll-down = ~/.config/polybar/pipewire.sh down &

;-------------------------------------------------i3
[module/i3]
type = internal/i3

;show monitor specific workspaces on different bars
pin-workspaces = true

; This will split the workspace name on ':'
; Default: false
strip-wsnumbers = true

; Sort the workspaces by index instead of the default
; sorting that groups the workspaces by output
; Default: false
index-sort = true

; Create click handler used to focus workspace
; Default: true
enable-click = false

; Create scroll handlers used to cycle workspaces
; Default: true
enable-scroll = false

; Wrap around when reaching the first/last workspace
; Default: true
wrapping-scroll = false

; Set the scroll cycle direction
; Default: true
reverse-scroll = false

; Use fuzzy (partial) matching on labels when assigning
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces
; containing 'code' in the label
; Default: false
fuzzy-match = true
; ws-icon-[0-9]+ = <label>;<icon>
; NOTE: The <label> needs to match the name of the i3 workspace
; Neither <label> nor <icon> can contain a semicolon (;)
; NOTE: You cannot skip icons, e.g. to get a ws-icon-6
; you must also define a ws-icon-5.
; NOTE: Icon will be available as the %icon% token inside label-*

; Available tags:
;   <label-state> (default) - gets replaced with <label-(focused|unfocused|visible|urgent)>
;   <label-mode> (default)
format = <label-state> <label-mode>

; Available tokens:
;   %mode%
; Default: %mode%
label-mode = %mode%
label-mode-padding = 0
label-mode-background = #e60053

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-focused =%index%
label-focused-foreground = #ffffff
;label-focused-background = #cc734f96
label-focused-background = ${colors.main}
label-focused-underline = #fba922
label-focused-padding = 1

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-unfocused = %index%
label-unfocused-padding = 1
label-unfocused-background = ${colors.bg}

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-visible = %index%
label-visible-underline = #555555
label-visible-padding = 1

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-urgent = %index%
label-urgent-foreground = ${colors.bg}
label-urgent-background = ${colors.warning}
label-urgent-padding = 1

; Separator in between workspaces
label-separator = |
label-separator-padding = 0
label-separator-foreground = ${colors.secondary}
label-separator-background = ${colors.bg}

;-------------------------------------------------battery
[module/battery]
type = internal/battery

; This is useful in case the battery never reports 100% charge
full-at = 60

; Use the following command to list batteries and adapters:
; $ ls -1 /sys/class/power_supply/
battery = BAT0
adapter = ADP0

; If an inotify event haven't been reported in this many
; seconds, manually poll for new values.
;
; Needed as a fallback for systems that don't report events
; on sysfs/procfs.
;
; Disable polling by setting the interval to 0.
;
; Default: 5
poll-interval = 5

; see "man date" for details on how to format the time string
; NOTE: if you want to use syntax tags here you need to use %%{...}
; Default: %H:%M:%S
time-format = %H:%M

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-5 = 
ramp-capacity-6 = 
ramp-capacity-7 = 
ramp-capacity-8 = 

; Available tags:
;   <label-charging> (default)
format-charging = %{B#A0008000} <ramp-capacity> <label-charging> %{B-}

; Available tags:
;   <label-discharging> (default)
format-discharging = %{B#A0ff2052} <ramp-capacity> <label-discharging> %{B-}

; Available tags:
;   <label-full> (default)
;   <bar-capacity>
;   <ramp-capacity>

format-full = %{B#A07fff00} <ramp-capacity> <label-full> %{F-}%{B-}

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
;   %time%
;   %consumption% (shows current charge rate in watts)
label-charging = %percentage%%

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
;   %time%
;   %consumption% (shows current discharge rate in watts)
label-discharging = %percentage%%

; Available tokens:
;   %percentage% (default) - is set to 100 if full-at is reached
;   %percentage_raw%
label-full = F 
