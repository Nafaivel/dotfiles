#!/bin/zsh
# foot --server &
mpd &
mpDris2 &
playerctld &
lxsession &
/usr/lib/kdeconnectd &
kdeconnect-indicator &
kwalletd5 &
otd &
mako &
emacs --daemon &
# ~/.dots/.azotempv &
~/.azotebg &
~/.config/waybar/launch.sh &
syncthing serve --no-browser > ~/.config/syncthing/syncthing.log &
cd $HOME/.config/startpage && python -m http.server 6868 &
sleep 2 && $HOME/.config/xob/site/brightness-watcher.py | wob &
sleep 20 && $HOME/.config/xob/site/pipewire-volume-watcher.py | wob &
swayidle -w \
	timeout 300 'for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlr-randr --output "${o}" --toggle; done;' \
		resume 'for o in $(wlr-randr | grep -e "^[^[:space:]]\+" | cut -d " " -f 1); do wlr-randr --output "${o}" --toggle; done;' \
	before-sleep 'playerctl pause' \
        before-sleep '~/.config/wayland_scripts/lock.sh'
        # timeout 300 '~/.config/wayland_scripts/lock.sh' \
