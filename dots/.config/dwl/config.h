/* appearance */
static const int sloppyfocus               = 1;  /* focus follows mouse */
static const int bypass_surface_visibility = 0;  /* 1 means idle inhibitors will disable idle tracking even if it's surface isn't visible  */
static const int smartborders              = 1;
static const unsigned int borderpx         = 1;  /* border pixel of windows */
static const float bordercolor[]           = {0.2, 0.14, 0.10, 0.0};
static const float focuscolor[]            = {0.5, 0.5, 0.5, 1.0};
/* To conform the xdg-protocol, set the alpha to zero to restore the old behavior */
static const float fullscreen_bg[]         = {0.1, 0.1, 0.1, 1.0};

/* tagging */
#define TAGCOUNT (9)
static const int tagcount = 9;

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	*/
	{ "qutebrowser",  NULL,   1 << 6,       0,           -1 },
	{ "kotatogram",  NULL,   1 << 4,0,           -1 },
	{ "mpv",          NULL,   1 << 7,       0,           -1 },
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect                x    y */
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
	*/
	/* defaults */
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
  .layout = "us,by",
  .variant = ",",
  .options = "grp:caps_toggle,grp:shifts_toggle,compose:menu,compose:ins",
};

static const int repeat_rate = 25;
static const int repeat_delay = 200;

/* Trackpad */
static const int tap_to_click = 1;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;
/* You can choose between:
LIBINPUT_CONFIG_SCROLL_NO_SCROLL
LIBINPUT_CONFIG_SCROLL_2FG
LIBINPUT_CONFIG_SCROLL_EDGE
LIBINPUT_CONFIG_SCROLL_ON_BUTTON_DOWN
*/
static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;

/* You can choose between:
LIBINPUT_CONFIG_CLICK_METHOD_NONE       
LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS       
LIBINPUT_CONFIG_CLICK_METHOD_CLICKFINGER 
*/
static const enum libinput_config_click_method click_method = LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS;

/* You can choose between:
LIBINPUT_CONFIG_SEND_EVENTS_ENABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED_ON_EXTERNAL_MOUSE
*/
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;

/* You can choose between:
LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT
LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE
*/
static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;
/* You can choose between:
LIBINPUT_CONFIG_TAP_MAP_LRM -- 1/2/3 finger tap maps to left/right/middle
LIBINPUT_CONFIG_TAP_MAP_LMR -- 1/2/3 finger tap maps to left/middle/right
*/
static const enum libinput_config_tap_button_map button_map = LIBINPUT_CONFIG_TAP_MAP_LRM;

/* If you want to use the windows key for MODKEY, use WLR_MODIFIER_LOGO */
// #define MODKEY WLR_MODIFIER_ALT
#define MODKEY WLR_MODIFIER_LOGO

#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, KEY,            tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,KEY,toggletag,  {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
// #define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/zsh", "-c", cmd, NULL } }

/* commands */
static const char lf[] = "(cat ~/.cache/wal/sequences && printf %b '\\e]11;#000000\a' &) && lf";
static const char kastler[] = "(cat ~/.cache/wal/sequences && printf %b '\\e]11;#000000\a' &) && kastler";
static const char pulsemixer[] = "(cat ~/.cache/wal/sequences && printf %b '\\e]11;#000000\a' &) && pulsemixer";
static const char terminal[] = "alacritty";
static const char *termcmd[] = { terminal, NULL };
static const char *fmclicmd[] = { terminal, "-e", "zsh", "-c", lf};
static const char *kastlercmd[] = { terminal, "-e", "zsh", "-c", kastler};
static const char *pulsemixercmd[] = { terminal, "-e", "zsh", "-c", pulsemixer};
static const char *fmcmd[] = { "thunar", NULL };
static const char *browser[] = { "qutebrowser", NULL };
static const char *alteditorcmd[] = { terminal, "-e", "nvim", NULL };
static const char *editorcmd[] = { "emacsclient", "--create-frame", NULL };
static const char *lockcmd[] = { "zsh", "-c", "$HOME/.config/wayland_scripts/lock.sh"};

//menu
static const char *menu[] = { "wofi", "--show", "drun", "--allow-images", "--insensetive", NULL };
static const char *zathura_menu[] = {"zsh", "-c", "~/.config/zathura/hstory_run.sh", NULL};
static const char *nightdayamode_menu[] = {"zsh", "-c", "~/.config/rofi/Scripts/night_day_mode.sh", NULL};
static const char *keys_menu[] = {"zsh", "-c", "~/.config/rofi/Scripts/keys.sh", NULL};
static const char *players_menu[] = {"zsh", "-c", "~/.config/rofi/Scripts/playerSwitch.py", NULL};
static const char *bluetooth_menu[] = {"zsh", "-c", "~/.config/rofi/Scripts/dmenu-bluetooth", NULL};
static const char *network_menu[] = {"zsh", "-c", "networkmanager_dmenu", NULL};
static const char *music_menu[] = {"zsh", "-c", "~/.config/rofi/Scripts/rofi-beats", NULL};

// playerctl cmds
static const char *playerctl_playpause_cmd[] = {"playerctl", "-i", "kdeconnect,mpd", "play-pause", NULL};
static const char *playerctl_next_cmd[] = {"playerctl", "-i", "mpd,kdeconnect", "next", NULL};
static const char *playerctl_prev_cmd[] = {"playerctl", "-i", "mpd,kdeconnect", "previous", NULL};
// mpv
static const char *playerctl_mpd_playpause_cmd[] = {"playerctl", "-p", "mpd", "play-pause", NULL};
static const char *playerctl_mpd_next_cmd[] = {"playerctl", "-p", "mpd", "next", NULL};
static const char *playerctl_mpd_prev_cmd[] = {"playerctl", "-p", "mpd", "previous", NULL};
static const char *playerctl_mpd_volup_cmd[] = {"playerctl", "-p", "mpd", "volume", "0.05+", NULL};
static const char *playerctl_mpd_voldown_cmd[] = {"playerctl", "-p", "mpd", "volume", "0.05-", NULL};
// kdeconnect
static const char *playerctl_kdeconect_playpause_cmd[] = {"playerctl", "-p", "kdeconnect", "play-pause", NULL};
static const char *playerctl_kdeconect_next_cmd[] = {"playerctl", "-p", "kdeconnect", "next", NULL};
static const char *playerctl_kdeconect_prev_cmd[] = {"playerctl", "-p", "kdeconnect", "previous", NULL};
static const char *playerctl_kdeconect_volup_cmd[] = {"playerctl", "-p", "kdeconnect", "volume", "0.05+", NULL};
static const char *playerctl_kdeconect_voldown_cmd[] = {"playerctl", "-p", "kdeconnect", "volume", "0.05-", NULL};

// volume control
static const char *volupcmd[] = {"pactl", "set-sink-volume", "@DEFAULT_SINK@", "+3%", NULL};
static const char *voldowncmd[] = {"pactl", "set-sink-volume", "@DEFAULT_SINK@", "-3%", NULL};
static const char *volmutecmd[] = {"pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL};
static const char *micmutecmd[] = {"pactl", "set-source-mute", "@DEFAULT_SOURCE@", "toggle", NULL};

// brightness control cmd
static const char *brightness_upcmd[] = {"brightnessctl", "--device=amdgpu_bl0", "s", "5+", NULL};
static const char *brightness_downcmd[] = {"brightnessctl", "--device=amdgpu_bl0", "s", "5-", NULL};

// screenshots
static const char *screensavepartcmd[] = {"zsh", "-c", "$HOME/.config/wayland_scripts/screenshots/partsave.sh", NULL};
static const char *screensavefullcmd[] = {"zsh", "-c", "$HOME/.config/wayland_scripts/screenshots/fullsave.sh", NULL};
static const char *screenclipcmd[] = {"zsh", "-c", "$HOME/.config/wayland_scripts/screenshots/partclip.sh", NULL};

#include "keys.h"
static const Key keys[] = {
	/* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
	/* modifier                  key                 function        argument */
  //     ___                    
  //    /   |  ____  ____  _____
  //   / /| | / __ \/ __ \/ ___/
  //  / ___ |/ /_/ / /_/ (__  ) 
  // /_/  |_/ .___/ .___/____/  
  //       /_/   /_/            
	{ MODKEY,                    Key_Return,     spawn,          {.v = termcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_f,          spawn,          {.v = fmclicmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_n,          spawn,          {.v = kastlercmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT|WLR_MODIFIER_CTRL, Key_f, spawn, {.v = fmcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_w,          spawn,          {.v = browser} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_e,          spawn,          {.v = editorcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT|WLR_MODIFIER_CTRL, Key_e, spawn, {.v = alteditorcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_u,          spawn,          {.v = pulsemixercmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT|WLR_MODIFIER_CTRL, Key_l, spawn, {.v = lockcmd } },
  // menu
	{ MODKEY,                    Key_d,          spawn,          {.v = menu} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_r,          spawn,          {.v = zathura_menu} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_d,          spawn,          {.v = nightdayamode_menu} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_s,          spawn,          {.v = keys_menu} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_p,          spawn,          {.v = players_menu} },
	{ MODKEY|WLR_MODIFIER_ALT,   Key_n,          spawn,          {.v = network_menu} },
	{ MODKEY|WLR_MODIFIER_ALT,   Key_b,          spawn,          {.v = bluetooth_menu} },
	{ MODKEY|WLR_MODIFIER_ALT,   Key_m,          spawn,          {.v = music_menu } },

  //  _       ____  ___
  // | |     / /  |/  /
  // | | /| / / /|_/ / 
  // | |/ |/ / /  / /  
  // |__/|__/_/  /_/   
  // focuse
	{ MODKEY,                    Key_j,          focusstack,     {.i = +1} },
	{ MODKEY,                    Key_k,          focusstack,     {.i = -1} },
  // move 
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_j,          movestack,      {.i = +1} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_k,          movestack,      {.i = -1} },
  // incnmaster
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_l,          incnmaster,     {.i = +1} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_h,          incnmaster,     {.i = -1} },
  // resize
	{ MODKEY,                    Key_h,          setmfact,       {.f = -0.05} },
	{ MODKEY,                    Key_l,          setmfact,       {.f = +0.05} },
  // move to master
	{ MODKEY|WLR_MODIFIER_CTRL,  Key_Return,     zoom,           {0} },
  // prev tag
	{ MODKEY,                    Key_Tab,        view,           {0} },
  // kill focused window
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_q,          killclient,     {0} },
  // layouts
	{ MODKEY,                    Key_t,          setlayout,      {.v = &layouts[0]} },
	// { MODKEY,                    Key_f,          setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                    Key_m,          setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                    Key_space,      setlayout,      {0} },
	{ MODKEY|WLR_MODIFIER_CTRL,  Key_space,      togglefloating, {0} },
	{ MODKEY,                    Key_f,          togglefullscreen, {0} },
	{ MODKEY,                    Key_0,          view,           {.ui = ~0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_parenright, tag,            {.ui = ~0} },
	{ MODKEY,                    Key_comma,      focusmon,       {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY,                    Key_period,     focusmon,       {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_comma,       tagmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, Key_period,    tagmon,         {.i = WLR_DIRECTION_RIGHT} },
	TAGKEYS(                     Key_1,                       0),
	TAGKEYS(                     Key_2,                       1),
	TAGKEYS(                     Key_3,                       2),
	TAGKEYS(                     Key_4,                       3),
	TAGKEYS(                     Key_5,                       4),
	TAGKEYS(                     Key_6,                       5),
	TAGKEYS(                     Key_7,                       6),
	TAGKEYS(                     Key_8,                       7),
	TAGKEYS(                     Key_9,                       8),

	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,Key_BackSpace, quit, {0} },
#define CHVT(KEY,n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT, KEY, chvt, {.ui = (n)} }
	CHVT(Key_F1, 1), CHVT(Key_F2,  2),  CHVT(Key_F3,  3),  CHVT(Key_F4,  4),
	CHVT(Key_F5, 5), CHVT(Key_F6,  6),  CHVT(Key_F7,  7),  CHVT(Key_F8,  8),
	CHVT(Key_F9, 9), CHVT(Key_F10, 10), CHVT(Key_F11, 11), CHVT(Key_F12, 12),

  //    _____                                __          __ 
  //   / ___/_____________  ___  ____  _____/ /_  ____  / /_
  //   \__ \/ ___/ ___/ _ \/ _ \/ __ \/ ___/ __ \/ __ \/ __/
  //  ___/ / /__/ /  /  __/  __/ / / (__  ) / / / /_/ / /_  
  // /____/\___/_/   \___/\___/_/ /_/____/_/ /_/\____/\__/  
                                                       
  { 0, Key_Print, spawn, {.v = screenclipcmd } },
  { WLR_MODIFIER_CTRL, Key_Print, spawn, {.v = screensavefullcmd } },
  { WLR_MODIFIER_SHIFT, Key_Print, spawn, {.v = screensavepartcmd } },

  //  _    __      __                   
  // | |  / /___  / /_  ______ ___  ___ 
  // | | / / __ \/ / / / / __ `__ \/ _ \
  // | |/ / /_/ / / /_/ / / / / / /  __/
  // |___/\____/_/\__,_/_/ /_/ /_/\___/ 
  //                                   
  { 0, Key_XF86AudioRaiseVolume, spawn, {.v = volupcmd } },
  { 0, Key_XF86AudioLowerVolume, spawn, {.v = voldowncmd } },
  { 0, Key_XF86AudioMute,        spawn, {.v = volmutecmd } },
  { 0, Key_XF86AudioMicMute,     spawn, {.v = micmutecmd } },

  //     ____  __                           __  __
  //    / __ \/ /___ ___  _____  __________/ /_/ /
  //   / /_/ / / __ `/ / / / _ \/ ___/ ___/ __/ / 
  //  / ____/ / /_/ / /_/ /  __/ /  / /__/ /_/ /  
  // /_/   /_/\__,_/\__, /\___/_/   \___/\__/_/   
  //               /____/                        
  {0, Key_Pause, spawn, {.v = playerctl_playpause_cmd }},
  {0, Key_XF86AudioPlay, spawn, {.v = playerctl_playpause_cmd }},
  {0, Key_XF86AudioNext, spawn, {.v = playerctl_next_cmd }},
  {0, Key_XF86AudioPrev, spawn, {.v = playerctl_prev_cmd }},

  // MPD
  { MODKEY, Key_Pause, spawn, { .v = playerctl_mpd_playpause_cmd }},
  { MODKEY, Key_XF86AudioPlay, spawn, { .v = playerctl_mpd_playpause_cmd }},
  { MODKEY, Key_XF86AudioNext, spawn, { .v = playerctl_mpd_next_cmd }},
  { MODKEY, Key_XF86AudioPrev, spawn, { .v = playerctl_mpd_prev_cmd }},
  { MODKEY, Key_XF86AudioRaiseVolume, spawn, { .v = playerctl_mpd_volup_cmd }},
  { MODKEY, Key_XF86AudioLowerVolume, spawn, { .v = playerctl_mpd_voldown_cmd }},

  // kdeconnect
  { WLR_MODIFIER_CTRL, Key_Pause, spawn, { .v = playerctl_kdeconect_playpause_cmd }},
  { WLR_MODIFIER_CTRL, Key_XF86AudioPlay, spawn, { .v = playerctl_kdeconect_playpause_cmd }},
  { WLR_MODIFIER_CTRL, Key_XF86AudioNext, spawn, { .v = playerctl_kdeconect_next_cmd }},
  { WLR_MODIFIER_CTRL, Key_XF86AudioPrev, spawn, { .v = playerctl_kdeconect_prev_cmd }},
  { WLR_MODIFIER_CTRL, Key_XF86AudioRaiseVolume, spawn, { .v = playerctl_kdeconect_volup_cmd }},
  { WLR_MODIFIER_CTRL, Key_XF86AudioLowerVolume, spawn, { .v = playerctl_kdeconect_voldown_cmd }},

  //     ____       _       __    __                      
  //    / __ )_____(_)___ _/ /_  / /_____  ___  __________
  //   / __  / ___/ / __ `/ __ \/ __/ __ \/ _ \/ ___/ ___/
  //  / /_/ / /  / / /_/ / / / / /_/ / / /  __(__  |__  ) 
  // /_____/_/  /_/\__, /_/ /_/\__/_/ /_/\___/____/____/  
  //              /____/                                  
  {0, Key_XF86MonBrightnessUp,                 spawn, {.v = brightness_upcmd}},
  {0, Key_XF86MonBrightnessDown,               spawn, {.v = brightness_downcmd}},
  {MODKEY|WLR_MODIFIER_CTRL, Key_bracketright, spawn, {.v = brightness_upcmd}},
  {MODKEY|WLR_MODIFIER_CTRL, Key_bracketleft,  spawn, {.v = brightness_downcmd}},

};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};

// autostart
static const char *const autostart[] = {
  "zsh", "-c", "~/.config/dwl/scripts/autostart.sh", NULL,
	// "foot", "--server", NULL,
 //  "mpd", NULL,
 //  "mpDris2", NULL,
 //  "playerctld", NULL,
 //  "lxsession", NULL,
 //  "/usr/lib/kdeconnectd", NULL,
 //  "indicator-kdeconnect", NULL,
 //  // "libinput-gestures", NULL,
 //  "kwalletd5", NULL,
 //  "otd", NULL,
 //  "mako", NULL,
 //  "zsh", "-c", "~/.dots/.azotempv", NULL,
 //  "zsh", "-c", "~/.config/waybar/launch.sh", NULL,
 //  "zsh", "-c", "syncthing", "serve", "--no-browser", ">", "~/.config/syncthing/syncthing.log", NULL,
 //  "zsh", "-c", "cd", "$HOME/.config/startpage", "&&", "python", "-m", "http.server", "6868", NULL,
 //  "zsh", "-c", "$HOME/.config/xob/site/brightness-watcher.py", "|", "wob", NULL,
 //  "zsh", "-c", "$HOME/.config/xob/site/pipewire-volume-watcher.py", "|", "wob", NULL,
	NULL
};
