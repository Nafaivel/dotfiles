#################################################
#  _    __           _       __    __
# | |  / /___ ______(_)___ _/ /_  / /__  _____
# | | / / __ `/ ___/ / __ `/ __ \/ / _ \/ ___/
# | |/ / /_/ / /  / / /_/ / /_/ / /  __(__  )
# |___/\__,_/_/  /_/\__,_/_.___/_/\___/____/
#################################################

# Fix zsh
# interpreter for shell commands
set shell zsh
# set '-eu' options for shell commands
set shellflag '-c'
set shellopts '-euy'
# set internal field separator (IFS) to "\n" for shell commands
set ifs "\n"
# default already
set filesep "\n" 

# file icons
set icons

# sixel
set sixel true

# image preview
set previewer ~/.config/lf/preview
set cleaner lf-cleaner

# leave some space at the top and the bottom of the screen
set scrolloff 10

# auto refresh with period (don't need to <C-r> to update dir content)
set period 1

#################################################
#     __ __
#    / //_/__  __  ______ ___  ____ _____  _____
#   / ,< / _ \/ / / / __ `__ \/ __ `/ __ \/ ___/
#  / /| /  __/ /_/ / / / / / / /_/ / /_/ (__  )
# /_/ |_\___/\__, /_/ /_/ /_/\__,_/ .___/____/
#           /____/               /_/
#################################################

# use enter for shell commands
map <enter> shell

# navigation
map J half-down
map K half-up
map ] :updir; set dironly true; down; set dironly false; open
map [ :updir; set dironly true; up; set dironly false; open

# run shell
map S $zsh

# delete, cut
map d
map dD delete -- $fs
map dd cut -- $fs .

# yank
map yy copy -- $fs .
map yn yank-basename
map y. yank-basename-without-extension
map yp yank-path
map yc yank-content
map yd yank-dirname
map y/ clear
map y

# find_search
map f find
map F fzf
map / search
map gs :fzf_search

# Quick mount
map UM ${{blkid -o list | rg -e "not mounted" | sed '/swap/d' | fzf | awk '{print $1;}' | xargs udisksctl mount -b}}
map UU ${{blkid -o list | rg -e "media" | sed '/swap/d' | fzf | awk '{print $1;}' | xargs udisksctl unmount -b}}

# quick show/hide hidden files
map <backspace2> set hidden!

# drag and drop
map <c-d> &dragon-drop -T -x -a $fx

# create new file
map o push :touch<space>
map O push :mkdir<space>
# show file
# map i $bat -- $f
map i info

# rename
map a rename
map r rename
map A &thunar --bulk-rename ./*
map R &thunar --bulk-rename ./*

# open
map L open-with
map E &nohup $TERMINAL -e $EDITOR $f > /dev/null &

# open new lf in existing folder
map gN &$TERMINAL -e zsh -c "(cat ~/.cache/wal/sequences && printf %b '\\e]11;#000000\a' &) && lf"

# go to
map g/   cd /
map gG   cd ~/git_downloads
map gn   cd ~/Documents/notes
map gb   cd ~/books
map gc   cd ~/.config
map gde  cd ~/development
map gdoc cd ~/Documents
map gdot cd ~/.dots/
map gdow cd ~/Downloads
map ge   cd /etc
map gh   cd ~
map gm/   cd /mnt
map gmm   cd /run/media
map gmu   cd /run/user/1000/gvfs/
map gM   cd ~/Music/
map gps  cd ~/Pictures/Screenshots/
map gpw  cd ~/Pictures/walls/
map gpp  cd ~/Pictures
map gt   &nohup $TERMINAL > /dev/null &
map gv   cd ~/Videos
map g.l  cd ~/.local
map g.s  cd ~/.local/share
map g.d  cd ~/.dots/
map g.c  cd ~/.config/
map g.C  cd ~/.cache/
map g.h  cd ~/.HDD/

# toggle
map zp toggle_preview

# sort
map st :set sortby time; set reverse
map sT :set sortby time; set reverse!
map sr :set reverse!
map sR :set reverse
# set
map sw :set_wall 

##########################################################
#    ______                                          __
#   / ____/___  ____ ___  ____ ___  ____ _____  ____/ /____
#  / /   / __ \/ __ `__ \/ __ `__ \/ __ `/ __ \/ __  / ___/
# / /___/ /_/ / / / / / / / / / / / /_/ / / / / /_/ (__  )
# \____/\____/_/ /_/ /_/_/ /_/ /_/\__,_/_/ /_/\__,_/____/
#########################################################


# exit from lf
cmd q quit

# define a custom 'open' command
# This command is called when current file is not a directory. You may want to
# use either file extensions and/or mime types here. Below uses an editor for
# text files and a file opener for the rest.
cmd open $set -f; rifle -p 0 "$fx"
cmd open-with ${{
    clear
    set -f
    rifle -l $fx | sed -e "s/:[a-Z]*:[a-Z]*:/ \| /"
    read -k method                                                                                
    if [[ $method == $'\n' ]]; then                                                               
      method=0                                                                                    
    fi                                                                                            
    rifle -p $method $fx
}}

# cmd open ${{
#   if [[ ($f == *fb2.zip) || ($f == *fb2) ]]
#   then
#     nohup foliate $f > /dev/null &
#   else
#     echo $f
#     test -L $f && f=$(readlink -f $f)
#     case $(file --mime-type $f -b) in
#       # open in editor text files
#       text/*|application/json|application/x-shellscript|application/javascript|inode/x-empty) $EDITOR $fx;;
#       # office docs
#       application/vnd.oasis.opendocument.text|application/vnd.openxmlformats-officedocument.wordprocessingml.document) libreoffice $fx &> /dev/null &;;
#       # *) for f in $fx; do setsid -a $OPENER $f > /dev/null 2> /dev/null & done;;
#       # *) for f in $fx; do setsid -a xdg-open $f > /dev/null 2> /dev/null & done;;
#       *) for f in $fx; do setsid xdg-open $f > /dev/null 2> /dev/null & done;;
#     esac
#   fi
# }}

# create dir, file
# cmd mkdir %mkdir "$@"
# cmd touch %touch "$@"
cmd mkdir %IFS=" "; mkdir -- "$*"
cmd touch %IFS=" "; touch -- "$*"

# yank something
# https://github.com/gokcehan/lf/wiki/Tips#yank-paths-into-your-clipboard 
cmd yank-dirname $dirname -- "$f" | head -c-1 | $CLIP
cmd yank-path $printf '%s' "$fx" | $CLIP
cmd yank-basename $basename -a -- $fx | head -c-1 | $CLIP
cmd yank-content ${{cat $fx | $CLIP}}

cmd yank-basename-without-extension ${{
    echo "$fx" |
      xargs -r -d '\n' basename -a |
      awk -e '{
        for (i=length($0); i > 0; i--) {
          if (substr($0, i, 1) == ".") {
            if (i == 1) print $0
            else print substr($0, 0, i-1)

            break
          }
        }

        if (i == 0)
          print $0
      }' |
      if [ -n "$fs" ]; then cat; else tr -d '\n'; fi |
      $CLIP
}}

cmd yank-clear %{{
    # files=$(lf -remote load | tail -n +2)
    # newline=$'\n'
    #
    # # change to $fx to add current file when no toggled
    # # files exist.
    # if [ -n "$files" ]; then
    #     new_files=$(echo "$files${newline}$fs" | sort | uniq)
    # else
    #     new_files=$fs
    # fi
    # # remove empty lines from the file list, because they keep messing
    # # up the selection.
    # new_files=$(echo "$new_files" | sed --quiet -e '/^$/d' -e 'p')
    #
    # lf -remote "save${newline}move${newline}${new_files}${newline}"
    # lf -remote "send $id unselect${newline}send $id sync"
}}

# define a custom 'rename' command without prompt for overwrite
# cmd rename %[ -e $1 ] && printf "file exists" || mv $f $1
# map r push :rename<space>

# make sure trash folder exists
# %mkdir -p ~/.trash

# move current file or selected files to trash folder
# (also see 'man mv' for backup/overwrite options)
cmd trash %set -f; mv $fx ~/.trash

# define a custom 'delete' command
cmd delete ${{
    tput smcup
    clear
    set -f
    printf "$fx\n"
    printf "delete? [y/n] "
    read -k ans
    [[ $ans = "y" || $ans == "Y" ]] && nohup rm -rf $fx > /dev/null &
    tput smcup
}}

# use '<delete>' key for either 'trash' or 'delete' command
# map <delete> trash
# map <delete> delete

# extract the current file with the right command
# (xkcd link: https://xkcd.com/1168/)
cmd extract ${{
    set -f
    case $f in
        *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf $f;;
        *.tar.gz|*.tgz) tar xzvf $f;;
        *.tar.xz|*.txz) tar xJvf $f;;
        *.tar.zst) tar -xf $f;;
        *.zip) unzip $f;;
        *.rar) unrar-free x $f;;
        *.7z) 7z x $f;;
    esac
}}

cmd info ${{
    tput smcup
    set -f
    pager=("bat" "--color=always" "--pager" "less -R")

    case $(file --mime-type $f -b) in
      inode/directory) lsd --color=always -l $f | $pager -p && return 0;;
      *) echo $f;;
    esac

    case $f in
        *.cbz) zathura $f &;;
        *.pdf|*.doc|*.docx|*.epub|*.fb2) zaread $f &;;
        *.bmp|*.jpg|*.jpeg|*.png|*.xpm|*.webp|*.tiff|*.gif|*.jfif|*.ico|*.avif|*.svg) clear && chafa -f sixel $f -s x20 && echo "\n" && exiv2 $f 2>/dev/null | head -n5 && echo "\nperess any key to exit:" && read -k;;
        *) $pager -- $f;;
    esac
    tput rmcup
}}

# compress current file or selected files with tar and gunzip
cmd tar &{{
    set -f
    mkdir $1
    cp -r --target-directory=$1 $fx
    tar czf $1.tar.gz $1
    rm -rf $1
}}

# compress current file or selected files with zip
cmd zip &{{
    set -f
    mkdir $1
    cp -r --target-directory=$1 $fx
    zip -r $1.zip $1 > /dev/null
    rm -rf $1
}}

# compress current file or selected files with zip
cmd 7z &{{
    set -f
    mkdir $1
    cp -r --target-directory=$1 $fx
    7z a $1.7z $1
    rm -rf $1
}}

cmd fzf ${{
    file=$(fzf)
    lf -remote "send $id select \"$file\""
}}

cmd toggle_preview %{{
    if [ "$lf_preview" = "true" ]; then
        lf -remote "send $id :set preview false; set ratios 1:5"
    else
        lf -remote "send $id :set preview true; set ratios 1:2:3"
    fi
}}

# https://github.com/gokcehan/lf/wiki/Tips#bulk-rename-multiple-files 
cmd bulk-rename ${{
    old="$(mktemp)"
    new="$(mktemp)"
    if [ -n "$fs" ]; then
        fs="$(basename -a $fs)"
    else
        fs="$(ls)"
    fi
    printf '%s
' "$fs" >"$old"
    printf '%s
' "$fs" >"$new"
    $EDITOR "$new"
    [ "$(wc -l < "$new")" -ne "$(wc -l < "$old")" ] && exit
    paste "$old" "$new" | while IFS= read -r names; do
        src="$(printf '%s' "$names" | cut -f1)"
        dst="$(printf '%s' "$names" | cut -f2)"
        if [ "$src" = "$dst" ] || [ -e "$dst" ]; then
            continue
        fi
        mv -- "$src" "$dst"
    done
    rm -- "$old" "$new"
    lf -remote "send $id unselect"
}}

# https://github.com/gokcehan/lf/wiki/Tips#show-current-directory-in-window-title 
on-cd
cmd on-cd &{{
    # '&' commands run silently in background (which is what we want here),
    # but are not connected to stdout.
    # To make sure our escape sequence still reaches stdout we pipe it to /dev/tty
    printf "]0; $PWD" > /dev/tty
}}

# https://github.com/gokcehan/lf/wiki/Tips#warn-about-nested-instances 
%[ $LF_LEVEL -eq 1 ] || echo "Warning: lf level $LF_LEVEL"

# set wall
cmd set_wall &{{
  local file
  file="$HOME/.azotebg"

  local content
  content=$(cat "$file")
  echo $content

  # replace the string after the -i flag with the value of $f
  local new_content
  new_content=$(echo "$content" | sed "s|\(-i\s*\)\"[^\"]*\"|\1\"$f\"|")

  # write the new contents back to the file
  echo "$new_content" > "$file"

  # nohup ~/.dots/.azotempv
  ~/.azotebg
}}

# to html
cmd html &{{
    pandoc --to html --output=output.html $f
    # case $f in *.doc) ; doc=true; echo "converted";; esac
    # if [ -z $doc ]
    # then do
    #     pandoc --to html --output=output.html $f
    # else
    #     pandoc --to html --output=output.html $f\x
    # fi
}}
cmd lhtml &{{
    libreoffice --convert-to html $f
}}

# convert to docx
# mostly used for converting docx to html, because pandoc not support doc
cmd dox &{{
    libreoffice --convert-to docx $f
}}
# convert to pdf 
cmd pdf &{{
    libreoffice --convert-to pdf $f
}}

cmd fzf_search ${{
    cmd="rg --column --line-number --no-heading --color=always --smart-case"

    calculate_bat_center_line() {
      echo "work"
      echo $1
    }

    fzf --ansi --disabled --layout=reverse --header="Search in files" --delimiter=: \
        --bind="start:reload([ -n {q} ] && $cmd -- {q} || true)" \
        --bind="change:reload([ -n {q} ] && $cmd -- {q} || true)" \
        --bind='enter:become(lf -remote "send $id select \"$(pwd)/$(echo {1})\"")' \
        --preview="fzf-bat-preview {1} {2}" \
        #--preview="bat --color=always --highlight-line={2} --pager=\"less +50G\" -- {1}" \
        #--preview-window='+{2}-/2' \
        # Alternatively you can even use the same previewer you've configured for lf
        #--preview='~/.config/lf/cleaner; ~/.config/lf/previewer {1} "$FZF_PREVIEW_COLUMNS" "$FZF_PREVIEW_LINES" "$FZF_PREVIEW_LEFT" "$FZF_PREVIEW_TOP"'
}}
