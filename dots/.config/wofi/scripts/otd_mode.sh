#!/usr/bin/bash

declare -a options=(
"drawing"
"osu"
"daily"
)

choise=$(printf '%s\n' "${options[@]}" | wofi --dmenu -i -l 4 -p "choose mode:")

if [ "$choise" = "drawing" ]; then
  otd loadsettings $HOME/.dots/devices/tablet_otd/drawing.json
elif [ "$choise" = "osu" ]; then
  otd loadsettings $HOME/.dots/devices/tablet_otd/osu
elif [ "$choise" = "daily" ]; then
  otd loadsettings $HOME/.dots/devices/tablet_otd/daily.json
else
  echo "OK!" && exit 1
fi

