#!/usr/bin/bash

main() {
  scripts=$(ls -R ~/development/game_scripts | awk '/:$/&&f{s=$0;f=0}/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}NF&&f{ print s"/"$0 }')

  declare -a options=(
  "$scripts"
  "quit"

  )

  choise=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'virt')


  if [[ "$choise" == quit ]]; then
      echo "$scripts" && exit 1
  elif [ "$choise" ]; then
      cfg=$(printf '%s\n' "$choise" | awk '{print $NF}')
      "$cfg"
  else
      echo "OK!" && exit 1
  fi
}

pkill -f game_scripts || main
