#!/bin/bash

pack="paru --color=always --sudoloop --cleanafter --noconfirm --needed --useask"
root=sudo
noconfirm=$1

_confirm()
{
  if [[ $noconfirm = "a" ]]
  then
    echo
    return 0
  fi

  echo -n "$@ "
  read -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    return 0
  else
    return 1
  fi
}

add() {
  echo -n ">> install $1: " \
    && shift \
    && _confirm $@ \
    && $pack -S $@ \
    && echo
}

confirm() {
  echo -n ">> " \
    && _confirm $@ \
    && echo
}

echo "install packages"
echo 'choice marked by ">> "'
echo "confirm by pressing Y/y any other key is considered as no"

add "build tools" git gcc cmake lld clang ninja rustup llvm
add "utils" doas fzf sudo rg lsd brightnessctl translate-shell
confirm "set rustup nightly" && rustup default nightly
add "langs" go python luarocks poetry rustup zig
add "editors" \
  neovim-git npm yarn perl python-pynvim tree-sitter \
  emacs-wayland
confirm "clone nvim config" && git clone https://gitlab.com/Nafaivel/nvim_config ~/.config/nvim
confirm "clone emacs config" && git clone https://gitlab.com/Nafaivel/emacs_cfg ~/.config/emacs
confirm "rm .emacs.d" && rm -r ~/.emacs.d
add "thunar" thunar thunar-archive-plugin engrampa thunar-volman gvfs gvfs-mtp
add "shell" zsh{,-history-substring-search,-autosuggestions,-theme-powerlevel10k-bin-git,-syntax-highlighting}
add "terminals" foot alacritty
add "lf" lf dragon-drop unzip unrar-free tar p7zip ranger chafa
add "book readers" zathura zathura-cb zathura-pdf-mupdf pandoc-cli calibre foliate
add "office" libreoffice-fresh
add "fonts" ttf-{inconsolata-lgc-nerd,iosevka-lyte-nerd-font,liberation}
add "browsers" qutebrowser librewolf-bin
add "spell" hunspell{,-be-tarask,-en_us,-ru}
add "audio" pipewire{,-pulse,-alsa} pulsemixer pavucontrol playerctl
add "network" networkmanager{,-dmenu-git,-openvpn} iwd wireguard-tools nm-connection-editor
add "llm" ollama shell-gpt
add "plot" gnuplot
add "river" river kile-wl
add "look" azote python-pywal16 nwg-look qt{5,6}ct
add "themes" papirus{,-folders} dracula-gtk-theme
confirm "set folders color to violet" && $root papirus-folders -C violet
add "wayland" wofi xdg-desktop-portal-wlr wlsunset wlr-randr wl-clipboard wdisplays wayvnc waybar qt{5,6}-wayland swaybg swayidle swaylock wev
add "sync" kdeconnect syncthing
add "bluetooth" blueman bluez{,-tools}
add "eww" eww dotnet-sdk yuckls-git
add "otd" opentabletdriver
confirm "blacklist wacom and hid_uclogic (used to fix otd)" && echo -ne "blacklist hid_uclogic \nblacklist wacom" | $root tee /etc/modprobe.d/otd_fix.conf
add "gpu deps" {lib32-,}vulkan-radeon {lib32-,}amdvlk {lib32-,}vulkan-mesa-layers {lib32-,}mesa {lib32-,}vkd3d
add "nvidia" nvidia-open-dkms {lib32-,}nvidia-utils nvidia-prime
add "native games?" osu-lazer-bin cataclysm-dda cataclysm-dda-tiles
add "wine games" lutris winetricks wine{,-gecko,-mono}
add "img display" feh imv
add "video" mpv
add "mpd" mpd mpd-mpris ncmpcpp
add "image editing" gimp krita
add "tesseract" tesseract tesseract-data-{bel,eng,ces}
add "polkit" lxsession
